$sourcePath = 'HKLM:\SOFTWARE\Microsoft\Speech\Voices\Tokens'
$sourcePath32 = 'HKLM:\SOFTWARE\WOW6432Node\Microsoft\SPEECH\Voices\Tokens'
$destinationPath = 'HKLM:\SOFTWARE\Microsoft\Speech_OneCore\Voices\Tokens'

#cleanup
$listOldVoices = Get-ChildItem $destinationPath
foreach($voice in $listOldVoices)
{
    if ($voice.PSChildName.StartsWith("TTS_") -or $voice.PSChildName.StartsWith("IVONA 2")) {
        $path = $voice.PSPath
        Remove-Item -Path $path -Recurse
    }
}

$listVoices = Get-ChildItem $sourcePath
foreach($voice in $listVoices)
{
    $source = $voice.PSPath
    copy -Path $source -Destination $destinationPath -Recurse
}
$listVoices = Get-ChildItem $sourcePath32
foreach($voice in $listVoices)
{
    $source = $voice.PSPath
    copy -Path $source -Destination $destinationPath -Recurse
}